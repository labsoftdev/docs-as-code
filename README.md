--- 
copyright: 2023 LabSoftDev
doctype:   README
author:    Dr. Momcilo Krunic <momcilo.krunic@labsoft.dev>
---

# Micro V-Model

Templates used for development of micro V-model artifacts.

More detailed explenation about the whole software development process and doc as code approach can be found here:
https://momcilo_krunic.gitlab.io/elektronika_ir_elektrotechnika_2023/Documentation_as_Code_in_Automotive_System_Software_Engineering.pdf