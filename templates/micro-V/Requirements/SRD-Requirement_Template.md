﻿---

copyright: 2023 LabSoftDev
doctype:   SRD-Software_Requirement_Document
author:    Dr. Momcilo Krunic <momcilo.krunic@labsoft.dev>

---

# User Story 
As a [type of user], I want [some goal] so that [some reason].

---
## Requirement - Executable specification

**Given**:

**When**:

**Then**:

```yaml
ID:
category:
verification_criteria:
verification_method:
safety_level:
security_relevant:
```
---

