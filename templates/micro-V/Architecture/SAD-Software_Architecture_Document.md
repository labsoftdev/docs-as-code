---

title:     <name of the software> Architecture 
copyright: LabSoftDev
doctype:   SAD-Software_Architecture_Document
author:    Dr. Momcilo Krunic <momcilo.krunic@labsoft.dev>

---

- [1. Introduction and Goals](#1-introduction-and-goals)
  - [1.1. Requirements Overview](#11-requirements-overview)
  - [1.2. Quality Goals](#12-quality-goals)
  - [1.3. Stakeholders](#13-stakeholders)
- [2. Architecture Constraints](#2-architecture-constraints)
- [3. System Scope and Context](#3-system-scope-and-context)
  - [3.1. Business Context](#31-business-context)
  - [3.2. Technical Context](#32-technical-context)
- [4. Solution Strategy](#4-solution-strategy)
- [5. Building Block View](#5-building-block-view)
  - [5.1. System Context - C4](#51-system-context---c4)
  - [5.2. Containers - C3](#52-containers---c3)
  - [5.3. Components - C2](#53-components---c2)
- [6. Runtime View](#6-runtime-view)
- [7. Deployment View](#7-deployment-view)
  - [7.1. Infrastructure Level 1](#71-infrastructure-level-1)
  - [7.2. Infrastructure Level 2](#72-infrastructure-level-2)
- [8. Cross-cutting Concepts](#8-cross-cutting-concepts)
- [9. Architecture Decisions](#9-architecture-decisions)
- [10. Quality Requirements](#10-quality-requirements)
  - [10.1. Quality Tree](#101-quality-tree)
  - [10.2. Quality Scenarios](#102-quality-scenarios)
- [11. Risks and Technical Debts](#11-risks-and-technical-debts)
- [12. Glossary](#12-glossary)

---

# 1. Introduction and Goals

```Markdown
https://docs.arc42.org/section-1/ 
```


## 1.1. Requirements Overview 


## 1.2. Quality Goals 


## 1.3. Stakeholders 

---

# 2. Architecture Constraints

```Markdown
https://docs.arc42.org/section-2/
```

---

# 3. System Scope and Context 

```Markdown
https://docs.arc42.org/section-3/ 
```

## 3.1. Business Context 


## 3.2. Technical Context

---

# 4. Solution Strategy 

```Markdown
https://docs.arc42.org/section-4/ 
```

---

# 5. Building Block View 

```Markdown
https://docs.arc42.org/section-5/ 
```

```Markdown 
https://c4model.com 
```

## 5.1. System Context - C4 

## 5.2. Containers - C3 

## 5.3. Components - C2

---

# 6. Runtime View 

```Markdown
https://docs.arc42.org/section-6/ 
```

---

# 7. Deployment View 

```Markdown 
https://docs.arc42.org/section-7/ 
```

## 7.1. Infrastructure Level 1 

## 7.2. Infrastructure Level 2

---

# 8. Cross-cutting Concepts 

```Markdown
https://docs.arc42.org/section-8/ 
```


---

# 9. Architecture Decisions 

```Markdown
https://docs.arc42.org/section-9/ 
```

---

# 10. Quality Requirements 

```Markdown
https://docs.arc42.org/section-10/ 
```

## 10.1. Quality Tree 


## 10.2. Quality Scenarios 

---

# 11. Risks and Technical Debts

```Markdown
https://docs.arc42.org/section-11/ 
```

---

# 12. Glossary 

```Markdown 
https://docs.arc42.org/section-12/ 
```
